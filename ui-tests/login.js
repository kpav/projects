import {
	Selector
} from "testcafe";
import credentials from "../fixtures/credentials";

fixture `Login Page`.page `https://sprinkle-burn.glitch.me/`;

const fields = Selector('.mb2 input');
const login = Selector('button');
const successfulLogin = Selector('.pa4.black-80.content');
const incorrectCredentials = Selector('#login-error-box');

test("Can not login with correct credentials", async t => {
	await t
		.typeText(fields.withAttribute('type', 'email'), credentials.valid.email)
		.click(fields.withAttribute('type', 'password'))
		.typeText(fields.withAttribute('type', 'password'), credentials.valid.password + ' ')
		.click(login)
		.setTestSpeed(0.1)
		.expect(incorrectCredentials.textContent).eql('Credentials are incorrect');
});

test("Can login with correct credentials", async t => {
	await t
		.typeText(fields.withAttribute('type', 'email'), credentials.valid.email)
		.click(fields.withAttribute('type', 'password'))
		.typeText(fields.withAttribute('type', 'password'), credentials.valid.password)
		.click(login)
		.setTestSpeed(0.1)
		.expect(successfulLogin.textContent).contains('Welcome Dr I Test');
});

test("Can not login with incorrect credentials", async t => {
	await t
		.typeText(fields.withAttribute('type', 'email'), credentials.invalid.email)
		.click(fields.withAttribute('type', 'password'))
		.typeText(fields.withAttribute('type', 'password'), credentials.invalid.password)
		.click(login)
		.setTestSpeed(0.1)
		.expect(incorrectCredentials.textContent).eql('Credentials are incorrect');
});

test("Can not login without email", async t => {
	await t
		.typeText(fields.withAttribute('type', 'email'), ' ')
		.click(fields.withAttribute('type', 'password'))
		.typeText(fields.withAttribute('type', 'password'), credentials.valid.password)
		.click(login)
		.setTestSpeed(0.1)
		.expect(incorrectCredentials.textContent).eql('Credentials are incorrect');
});

test("Can not login without password", async t => {
	await t
		.typeText(fields.withAttribute('type', 'email'), credentials.valid.email)
		.click(fields.withAttribute('type', 'password'))
		.typeText(fields.withAttribute('type', 'password'), ' ')
		.click(login)
		.setTestSpeed(0.1)
		.expect(incorrectCredentials.textContent).eql('Credentials are incorrect');
});

test("Can not login without credentials", async t => {
	await t
		.typeText(fields.withAttribute('type', 'email'), ' ')
		.click(fields.withAttribute('type', 'password'))
		.typeText(fields.withAttribute('type', 'password'), ' ')
		.click(login)
		.setTestSpeed(0.1)
		.expect(incorrectCredentials.textContent).eql('Credentials are incorrect');
});

test("Can not login with password equals to 1", async t => {
	await t
		.typeText(fields.withAttribute('type', 'email'), credentials.valid.email)
		.click(fields.withAttribute('type', 'password'))
		.typeText(fields.withAttribute('type', 'password'), '1')
		.click(login)
		.setTestSpeed(0.1)
		.expect(incorrectCredentials.textContent).eql('Credentials are incorrect');
});